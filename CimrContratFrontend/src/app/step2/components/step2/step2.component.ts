import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedDataService } from 'src/app/common/common-service';
import { FileUploadService } from 'src/app/core/services/file-upload.service';
import { ReferentielService } from 'src/app/core/services/referentiel-api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.css']
})
export class Step2Component implements OnInit
{

  constructor(private route: ActivatedRoute,
    private router: Router,
    private sharedDataService: SharedDataService,
    private referentielService: ReferentielService,
    private uploadService: FileUploadService,
    private fb: FormBuilder) {
	
  }

  listCategoriesHabilitation : any;
  listModesPaiement : any;
  listProduits : any;

  indexSelected = -1;
  produitLibelle = '';
  

  selectedFiles: FileList;
  currentFile: File;
  urlUploadRIB = 'api/adhesion/uploadRIB';
  message = '';

  contrat : any = {}

  dateEffetSouscription : any;
  effectif : any;
  rib : any;
  titulaire : any;
  modePaiementId : any;
  produitId : any;
  delegataires : any;
  nombreDelegataires: number;

  ngOnInit(): void {
    this.getListCategoriesHabilitaion();
    this.getListModesPaiement();
    this.getListProduits();
    this.populateContrat();
  }

  async populateContrat(){
    
    if(this.sharedDataService.contrat != null ){

      var sharedContrat = this.sharedDataService.contrat;
      
      this.dateEffetSouscription = sharedContrat.dateEffetSouscription;
      this.effectif = sharedContrat.effectif; 
      this.rib = sharedContrat.rib;
      this.titulaire = sharedContrat.titulaire;
      this.modePaiementId = sharedContrat.modePaiementId;
      this.produitId = sharedContrat.produitId;
      this.delegataires = sharedContrat.delegataires;
    }
    
    
    if(sharedContrat == null || this.delegataires == null || this.delegataires.length == 0){
      this.addDelegataire();
    } 
  }

  getListCategoriesHabilitaion(){
    this.referentielService.listCategoriesHabilitation('categoriesHabilitation').subscribe(
      data =>
        this.listCategoriesHabilitation = data);
    console.log('list categories habilitation-----------'+this.listCategoriesHabilitation);
  }

  getListModesPaiement(){
    this.referentielService.listModesPaiement('modesPaiement').subscribe(
      data =>
        this.listModesPaiement = data);
    console.log('list modes paiement-----------'+this.listModesPaiement);
  }

  getListProduits(){
    this.referentielService.listProduits('produits').subscribe(
      data =>{
        console.log(data)
        this.listProduits = data
      });
    console.log('list produits -----------'+this.listProduits);
  }

  radioChecked(id : any,libelle: any, index: any){
    
    this.produitId = id;
    this.produitLibelle = libelle;
    this.indexSelected = index;
  }

  addDelegataire() {
    
    const delegataire = new Object({
      id: null,
      nom: null,
      prenom: null,
      cin: null,
      fonction: null,
      categorieHabilitationCode: null
    });

    if(this.delegataires == undefined || this.delegataires == null){
      this.delegataires = [];
    }
    

    this.delegataires.push(delegataire);
    
    this.nombreDelegataires++;
  }

  deleteDelagataire(index: any) {
    console.log(index)
    console.log(this.delegataires)
    this.delegataires.splice(index, 1);
    this.nombreDelegataires--;
  }

  modePaiementChecked(id : any, index: any){
    
    this.modePaiementId = id;
  }

  step3(){
    
    this.contrat.dateEffetSouscription = this.dateEffetSouscription;
    this.contrat.effectif = this.effectif; 
    this.contrat.rib = this.rib;
    this.contrat.titulaire = this.titulaire;
    this.contrat.modePaiementId = this.modePaiementId;
    this.contrat.produitId = this.produitId;
    this.contrat.delegataires = this.delegataires;
    
    this.sharedDataService.contrat = {};
    this.sharedDataService.contrat = this.contrat;
    
    this.router.navigate(['/step3']);
  }

  cancel(){
    
    this.sharedDataService.contrat = null;
    this.router.navigate(['/home']);
  }

  uploadRIB(event) {
    this.selectedFiles = event.target.files;
    this.message = '';
    this.currentFile = this.selectedFiles.item(0);
    console.log(this.currentFile)
    this.uploadService.upload(this.currentFile,this.urlUploadRIB).subscribe(
      event => {
        if (event instanceof HttpResponse) {
          console.log(event)
          if(event.body != null && event.body.errors != null && event.body.errors.length > 0){
            var listMessages = '';
            for(var i=0 ; i < event.body.errors.length ; i++){
              this.message += listMessages.concat(event.body.errors[i] + ' '); 
              console.log(i + ' : ' + this.message)
            }
            
            Swal.fire({
              title : 'Echec', 
              html : this.message , 
              icon : 'warning'
            })
          
          } else {
            var res = event.body;

            this.rib = res.rib;
            this.titulaire = res.titulaire;
          }
        }
      },
      err => {
        Swal.fire({
              title : 'Echéc de chargement', 
              html : 'La taille du fichier est trop grande !' , 
              icon : 'warning'
            })
      });
      
  
    this.selectedFiles = undefined;
    this.currentFile = undefined;
    
  }
  
}
