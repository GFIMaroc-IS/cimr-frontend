import { Step2RoutingModule } from './step2-routing.module';
import { NgModule } from '@angular/core';
import { Step2Component } from './components/step2/step2.component';
import { CoreModule } from '../core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [Step2Component],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    Step2RoutingModule
  ]
})
export class Step2Module { }