import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Step2Component } from './components/step2/step2.component';

const routes: Routes = [{path:'',component:Step2Component}];

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(routes),
            ReactiveFormsModule,
            FormsModule],
  exports: [RouterModule]
})
export class Step2RoutingModule { }