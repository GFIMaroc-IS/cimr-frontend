import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedDataService } from 'src/app/common/common-service';
import { ReferentielService } from 'src/app/core/services/referentiel-api.service';
import { AdhesionService } from 'src/app/core/services/service-adhesion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.css']
})
export class Step3Component implements OnInit
{
  numRC : any;
  raisonSociale : any;
  adresse : any;
  formeJuridique : any;
  ice : any;
  ifu : any;
  cnss : any;
  numTaxe : any;
  nomMandataire : any;
  prenomMandataire : any;
  cinMandataire : any;
  fonctMandataire : any;
  telMandataire : any;
  nomGestionnaire : any;
  prenomGestionnaire : any;
  emailGestionnaire : any;
  fonctGestionnaire : any;
  telGestionnaire : any;
  dateEffetSouscription: any;
  effectif: any;
  titulaire: any;
  rib : any;
  modePaiementId : any;
  modePaiementLibelle : any;
  produitId : any;
  produitLibelle : any;
  
  delegataires : any;
  listCategoriesHabilitation : any;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private sharedDataService: SharedDataService,
    private referentielService: ReferentielService,
    private adhesionService: AdhesionService) {
	
  }

  ngOnInit(): void {
    console.log('ngOnInit step3')
    this.populate();
    
  }

  async populate(){
    this.getListCategoriesHabilitaion();
    if(this.sharedDataService.adhesion != null){
      await this.populateAdhesion();
    }
    if(this.sharedDataService.contrat != null){
      await this.populateContrat();
    }
    console.log(this.delegataires)
    if(this.delegataires == null || this.delegataires == undefined || this.delegataires.length == 0){
      
      this.addDelegataire();
    }
  }

  async populateAdhesion(){
    var adhesionShared = this.sharedDataService.adhesion;
      
    this.numRC = adhesionShared.numRC ;
    this.raisonSociale = adhesionShared.raisonSociale ;
    this.adresse = adhesionShared.adresse;
    this.formeJuridique = adhesionShared.formeJuridique;
    this.ice = adhesionShared.ice;
    this.ifu = adhesionShared.ifu;
    this.cnss = adhesionShared.cnss;
    this.numTaxe = adhesionShared.numTaxe;
    this.nomMandataire = adhesionShared.nomMandataire;
    this.prenomMandataire = adhesionShared.prenomMandataire;
    this.cinMandataire = adhesionShared.cinMandataire;
    this.fonctMandataire = adhesionShared.fonctMandataire;
    this.telMandataire = adhesionShared.telMandataire;
    this.nomGestionnaire = adhesionShared.nomGestionnaire;
    this.prenomGestionnaire = adhesionShared.prenomGestionnaire;
    this.emailGestionnaire = adhesionShared.emailGestionnaire ;
    this.fonctGestionnaire = adhesionShared.fonctGestionnaire ;
    this.telGestionnaire = adhesionShared.telGestionnaire ;
  }

  async populateContrat(){
    var sharedContrat = this.sharedDataService.contrat;
      
      this.dateEffetSouscription = sharedContrat.dateEffetSouscription;
      this.effectif = sharedContrat.effectif; 
      this.rib = sharedContrat.rib;
      this.titulaire = sharedContrat.titulaire;
      this.modePaiementId = sharedContrat.modePaiementId;
      this.produitId = sharedContrat.produitId;
      this.delegataires = sharedContrat.delegataires;
    
    if(this.modePaiementId != null && this.modePaiementId != '' ){
      await this.findModePaiementByCode();
    }
    if(this.produitId != null && this.produitId != ''){
      await this.findProduitByCode();
    }
    
    
  }

  addDelegataire() {
    
    const delegataire = new Object({
      id: null,
      nom: null,
      prenom: null,
      cin: null,
      fonction: null,
      categorieHabilitationCode: null
    });

    if(this.delegataires == undefined || this.delegataires == null){
      this.delegataires = [];
    }
    this.delegataires.push(delegataire);
  }

  getListCategoriesHabilitaion(){
    this.referentielService.listCategoriesHabilitation('categoriesHabilitation').subscribe(
      data =>
        this.listCategoriesHabilitation = data);
    console.log('list categories habilitation-----------'+this.listCategoriesHabilitation);
  }

  async findModePaiementByCode(){
    this.referentielService.findModePaiementByCode('findModePaiement?id='+ this.modePaiementId).subscribe(
      (data: any) =>{
        if(data != null)
        this.modePaiementLibelle = data.libelle;
      });
  }

  async findProduitByCode(){
    this.referentielService.findProduitByCode('findProduit?id='+ this.produitId).subscribe(
      (data: any) =>{
        if(data != null)
        this.produitLibelle = data.libelle;
      });
  }

  step4(){

    console.log('validate')
    var obj : any = {}
    obj.adhesion = this.sharedDataService.adhesion;
    obj.contrat = this.sharedDataService.contrat;
    
    console.log(obj)
    this.adhesionService.validate("adhesion/validate" , obj).subscribe(
      (res: any) => {
        console.log(res)
        if(res != null && res.errors != null && res.errors.length > 0){
          // show errors
          Swal.fire('','','error');
        } else if(res != null && res.id != null){
          var id = res.id
          this.router.navigate(['/step4/print', id]);
        }
        
      },
      err => {
        throw err;
      });
    
  }

  cancel(){
    
    this.sharedDataService.adhesion = null;
    this.sharedDataService.contrat = null;
    this.router.navigate(['/home']);
  }
  
}
