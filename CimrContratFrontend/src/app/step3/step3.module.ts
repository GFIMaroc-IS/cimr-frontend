import { Step3RoutingModule } from './step3-routing.module';
import { NgModule } from '@angular/core';
import { Step3Component } from './components/step3/step3.component';
import { CoreModule } from '../core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [Step3Component],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    Step3RoutingModule
  ]
})
export class Step3Module { }