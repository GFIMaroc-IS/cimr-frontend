import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Step3Component } from './components/step3/step3.component';

const routes: Routes = [{path:'',component:Step3Component}];

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(routes),
            FormsModule,
            ReactiveFormsModule],
  exports: [RouterModule]
})
export class Step3RoutingModule { }