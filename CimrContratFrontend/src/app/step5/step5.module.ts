import { Step5RoutingModule } from './step5-routing.module';
import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { Step5Component } from './components/step5.component';

@NgModule({
  declarations: [Step5Component],
  imports: [
    CoreModule,
    Step5RoutingModule
  ]
})
export class Step5Module { }