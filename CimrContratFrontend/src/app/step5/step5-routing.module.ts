import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Step5Component } from './components/step5.component';

const routes: Routes = [{path:'',component:Step5Component},
                       {path:"print/:id",component:Step5Component}];

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(routes),
            FormsModule,
            ReactiveFormsModule],
  exports: [RouterModule]
})
export class Step5RoutingModule { }