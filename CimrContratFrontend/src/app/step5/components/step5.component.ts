import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PrintService } from 'src/app/core/services/print.service';
import { AdhesionService } from 'src/app/core/services/service-adhesion.service';

@Component({
  selector: 'app-step5',
  templateUrl: './step5.component.html',
  styleUrls: ['./step5.component.css']
})
export class Step5Component implements OnInit
{

  id : any = 0;
  signPageUrl : any = '';
  signId : any = '';
  signWindow : any;
  printUrl = 'http://courriertem.northeurope.cloudapp.azure.com/SignApi/signrequest?id=';

  ngOnInit(): void {
    this.id = sessionStorage.getItem("idAdhesion");
  }

  constructor(private adhesionService: AdhesionService, 
    private http: HttpClient, private printService: PrintService) {
  }

  signBulletin() {
    this.adhesionService.signBulletin("adhesion/signBulletin?id=" + this.id).subscribe(
      (res: any) => {
        console.log(res);
        if(res != null){
          this.signPageUrl = res.signPageUrl;
          this.signId = res.id;
          var fenetre = window.open(this.signPageUrl, "_blank", "resizable=no, toolbar=no, scrollbars=no, menubar=no, status=no, directories=no, location=no, width=1000, height=600, left=100" + " top=100 ").focus();
          
          var startTime   = new Date().getTime();
          var currentTime = new Date().getTime();
          var timeDifference = currentTime - startTime;
          console.log('1 * startTime : ' + startTime + ' - currentTime : ' + currentTime + ' - timeDifference : ' + timeDifference);
          while (timeDifference < 30000) {
              currentTime = new Date().getTime();
              timeDifference = currentTime - startTime;
          }
          console.log('2 * startTime : ' + startTime + ' - currentTime : ' + currentTime + ' - timeDifference : ' + timeDifference);
          this.printService.PrintPDF('adhesion/printBulletin',this.signId,'Bulletin.pdf');
          /*
          var timer = setInterval(() => {        	  
        	  if(win.closed) {
                  this.printService.PrintPDF('adhesion/printBulletin',this.signId,'Bulletin.pdf');
                  clearInterval(timer);
              }
          }, 1000);
          */
        }
        
      },
      err => {
        throw err;
      });
  }

  printBulletin(){
    console.log('print')
    this.http.get(this.printUrl+this.signId).subscribe(
      (res: any) => {
        console.log(res)
        if(res != null){

        }
        
      },
      err => {
        throw err;
      });
  }
  
}
