import { Injectable } from '@angular/core';
import { BaseApiService } from './base-api.service';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService extends BaseApiService {

  constructor(private httpClient:HttpClient) {
    super(httpClient);
   }

   upload(file: File,url:string): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    console.log('File : '+ file);
    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.baseUrl}`+url, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.httpClient.request(req);
  }

  getFiles(url:string): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}`+url);
  }

   
}
