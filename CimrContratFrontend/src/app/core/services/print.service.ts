import { Injectable } from '@angular/core';
import Swal from "sweetalert2";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PrintService {

  baseUrl:string=window["baseUrl"]+'api/';
  constructor(private http:HttpClient) {
  }


  PrintPDF(action:string,body: any,fileName:string) {
    Swal.showLoading ();
    var authorization = 'Bearer ' + sessionStorage.getItem("key");
    let headerOptions = new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/pdf'
         //"Authorization": authorization
        //   'Accept': 'application/octet-stream', // for excel file
    });
    let requestOptions = { headers: headerOptions, responseType: 'blob' as 'blob' };
    console.log(this.baseUrl+action);
    this.http.post(this.baseUrl+action, body, requestOptions).pipe(map((data: any) => {
        let blob = new Blob([data], {
          type: 'application/pdf' // must match the Accept type
            // type: 'application/octet-stream' // for excel 
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = fileName;
        link.target = 'unsafe-eval';
        link.click();
        window.URL.revokeObjectURL(link.href);

    })).subscribe((result: any) => {
      Swal.close();
    },
    (error)=>{
      Swal.fire('Erreur serveur', 'Erreur de serveur', 'error')
       Swal.hideLoading();
      }
    
    );
}
PrintExcel(action:string,body: any,fileName:string) {
  Swal.showLoading ();
  var authorization = 'Bearer ' + sessionStorage.getItem("key");
  let headerOptions = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/octet-stream'
       //"Authorization": authorization
      //   'Accept': 'application/octet-stream', // for excel file
  });
  let requestOptions = { headers: headerOptions, responseType: 'blob' as 'blob' };
  this.http.post(this.baseUrl+action, body, requestOptions).pipe(map((data: any) => {
      let blob = new Blob([data], {
        type: 'application/octet-stream'
         // must match the Accept type
          // type: 'application/octet-stream' // for excel 
      });
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = fileName;
      link.target = 'unsafe-eval';
      link.click();
      window.URL.revokeObjectURL(link.href);

  })).subscribe((result: any) => {
    Swal.fire('Downloaded','Fichier excel exporté avec succès!','success')
    Swal.hideLoading();
  },
  (error)=>{
    Swal.fire('Erreur serveur', 'erreur de serveur ,merci de contacter la DGI', 'error')
     Swal.hideLoading();
    }
  
  );
}
PrintText(action:string,body: any) {
  Swal.showLoading ();
  var authorization = 'Bearer ' + sessionStorage.getItem("key");
  let headerOptions = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/text'
       //"Authorization": authorization
      //   'Accept': 'application/octet-stream', // for excel file
  });
  let requestOptions = { headers: headerOptions, responseType: 'blob' as 'blob' };
  this.http.post(this.baseUrl+action, body, requestOptions).pipe(map((data: any) => {
      let blob = new Blob([data], {
        type: 'application/text'
         // must match the Accept type
          // type: 'application/octet-stream' // for excel 
      });
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = 'sampleTextFile.txt';
      link.target = 'unsafe-eval';
      link.click();
      window.URL.revokeObjectURL(link.href);

  })).subscribe((result: any) => {
    Swal.hideLoading();
  },
  (error)=>{
    Swal.fire('Erreur serveur', 'erreur de serveur ,merci de contacter la DGI', 'error')
     Swal.hideLoading();
    }
  
  );
}
}
