import { ErrorHandler, Injectable, Injector, isDevMode } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import Swal from "sweetalert2";
import { LoggingService } from './logging-service';
import { Router } from '@angular/router';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  // Error handling is important and needs to be loaded first.
  // Because of this we should manually inject the services with Injector.
  constructor(private injector: Injector, private router: Router) { }

  handleError(error: Error | HttpErrorResponse) {

    const logger = this.injector.get(LoggingService);

    let message = error.message;
    let stackTrace;
    console.log(error)
    if (error instanceof HttpErrorResponse) {
      
      if (error.status == 401) {
        
      } else if (error.status == 403) {
        
      } else if (error.status == 900) {
        
      } else {
        // Server Error
        
      }

    } else {
      // Client Error
      
    }


  }
}
