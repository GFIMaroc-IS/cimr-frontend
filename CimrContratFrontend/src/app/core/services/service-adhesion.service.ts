import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse, HttpParams, HttpHeaders} from '@angular/common/http';
import {BaseApiService} from './base-api.service';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdhesionService{
  
  baseUrl:string=window["baseUrl"]+'api/';
  constructor(
    private http:HttpClient,
    private baseApiService: BaseApiService) {
  }
  validate(action:string,body:any): Observable<HttpResponse<any>> {
    console.log("call "+action);
    return this.http.put<any>(this.baseUrl+action,body);
  }

  signBulletin(action:string): Observable<HttpResponse<any>> {
    console.log("call signBulletin action :  "+action);
    return this.http.get<any>(this.baseUrl+action);
  }
  
}


