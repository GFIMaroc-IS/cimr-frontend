import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CoreModule } from './core/core.module';
import { RouterModule } from '@angular/router';
import { HtppInterceptorService } from './core/services/htpp-interceptor.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { GlobalErrorHandler } from "./core/services/global-error-handler";
import { BasicAuthHtppInterceptorService } from './authentication/services/basic-auth-htpp-interceptor.service';
import { ServerErrorInterceptor } from './core/filters/server-error-interceptor';
import { RECAPTCHA_BASE_URL } from 'ng-recaptcha';
import { TokenHtppInterceptorService } from './authentication/services/token-htpp-interceptor.service';
import { AuthGuardService } from './authentication/services/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    CoreModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthHtppInterceptorService,
      multi: true
    },
    {
      provide:HTTP_INTERCEPTORS, 
      useClass:HtppInterceptorService, 
      multi:true
    },
    ,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorInterceptor,
      multi: true
    },
    {
      provide: RECAPTCHA_BASE_URL,
      useValue: 'https://recaptcha.net/recaptcha/api.js', // use recaptcha.net script source for some of our users
    }
    ,
    {
      provide: HTTP_INTERCEPTORS, useClass: TokenHtppInterceptorService, multi: true
    },
    {
      provide: AuthGuardService
    },
    { 
      provide: ErrorHandler, 
      useClass: GlobalErrorHandler 
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
