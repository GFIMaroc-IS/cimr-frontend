import { Step4RoutingModule } from './step4-routing.module';
import { NgModule } from '@angular/core';
import { Step4Component } from './components/step4/step4.component';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [Step4Component],
  imports: [
    CoreModule,
    Step4RoutingModule
  ]
})
export class Step4Module { }