import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PrintService } from 'src/app/core/services/print.service';

@Component({
  selector: 'app-step4',
  templateUrl: './step4.component.html',
  styleUrls: ['./step4.component.css']
})
export class Step4Component implements OnInit
{

  id : any = 0;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

  constructor(private printService: PrintService,
    private route: ActivatedRoute) {
	
  }

  printConvention() {
    this.printService.PrintPDF('adhesion/printConvention', this.id,'Convention.pdf');
  }
  
}
