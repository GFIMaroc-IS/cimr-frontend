import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Step4Component } from './components/step4/step4.component';

const routes: Routes = [{path:'',component:Step4Component},
                       {path:"print/:id",component:Step4Component}];

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(routes),
            FormsModule,
            ReactiveFormsModule],
  exports: [RouterModule]
})
export class Step4RoutingModule { }