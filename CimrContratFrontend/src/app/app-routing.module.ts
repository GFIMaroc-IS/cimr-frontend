import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './core/components/main-layout/main-layout.component';
import {AuthGuardService as AuthGuard} from './authentication/services/auth-guard.service';


const routes: Routes = [{path:'',component:MainLayoutComponent,
                        children:[
                          {path:'',loadChildren:() => import(`./home/home.module`).then(m => m.HomeModule)},
                          {path:'home',loadChildren:() => import(`./home/home.module`).then(m => m.HomeModule)},
                          {path:'step1',loadChildren:() => import(`./step1/step1.module`).then(m => m.Step1Module)},
                          {path:'step2',loadChildren:() => import(`./step2/step2.module`).then(m => m.Step2Module)},
                          {path:'step3',loadChildren:() => import(`./step3/step3.module`).then(m => m.Step3Module)},
                          {path:'step4',loadChildren:() => import(`./step4/step4.module`).then(m => m.Step4Module)},
                          {path:'login',loadChildren:() => import(`./authentication/authentication.module`).then(m => m.AuthenticationModule)},
                          {path:'step5',loadChildren:() => import(`./step5/step5.module`).then(m => m.Step5Module),canActivate:[AuthGuard]}        
                        ]}];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true, scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
