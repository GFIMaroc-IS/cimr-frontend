import { Step1RoutingModule } from './step1-routing.module';
import { NgModule } from '@angular/core';
import { Step1Component } from './components/step1/step1.component';
import { CoreModule } from '../core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [Step1Component],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    Step1RoutingModule
  ]
})
export class Step1Module { }