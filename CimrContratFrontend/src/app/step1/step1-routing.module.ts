import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Step1Component } from './components/step1/step1.component';

const routes: Routes = [{path:'',component:Step1Component}];

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(routes),
            FormsModule,
            ReactiveFormsModule],
  exports: [RouterModule]
})
export class Step1RoutingModule { }