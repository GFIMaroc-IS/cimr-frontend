import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedDataService } from 'src/app/common/common-service';
import { BaseApiService } from 'src/app/core/services/base-api.service';
import { FileUploadService } from 'src/app/core/services/file-upload.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.css']
})
export class Step1Component implements OnInit {
  numRC: any;
  raisonSociale: any;
  adresse: any;
  formeJuridique: any;
  ice: any;
  ifu: any;
  cnss: any;
  numTaxe: any;
  nomMandataire: any;
  prenomMandataire: any;
  cinMandataire: any;
  fonctMandataire: any;
  telMandataire: any;
  nomGestionnaire: any;
  prenomGestionnaire: any;
  emailGestionnaire: any;
  fonctGestionnaire: any;
  telGestionnaire: any;

  iceEmpty: boolean = false;
  ifuEmpty: boolean = false;
  cinEmpty: boolean = false;
  emailEmpty: boolean = false;

  requiredFielsEmpty: boolean = false;

  selectedFiles: FileList;
  currentFile: File;
  urlUploadRC = 'api/adhesion/uploadRC';
  urlUploadICE = 'api/adhesion/uploadICE';
  message = '';

  adhesion: any = {}

  ngOnInit(): void {
    console.log('init step1')

    if (this.sharedDataService.adhesion != null) {
      var adhesionShared = this.sharedDataService.adhesion;

      this.numRC = adhesionShared.numRC;
      this.raisonSociale = adhesionShared.raisonSociale;
      this.adresse = adhesionShared.adresse;
      this.formeJuridique = adhesionShared.formeJuridique;
      this.ice = adhesionShared.ice;
      this.ifu = adhesionShared.ifu;
      this.cnss = adhesionShared.cnss;
      this.numTaxe = adhesionShared.numTaxe;
      this.nomMandataire = adhesionShared.nomMandataire;
      this.prenomMandataire = adhesionShared.prenomMandataire;
      this.cinMandataire = adhesionShared.cinMandataire;
      this.fonctMandataire = adhesionShared.fonctMandataire;
      this.telMandataire = adhesionShared.telMandataire;
      this.nomGestionnaire = adhesionShared.nomGestionnaire;
      this.prenomGestionnaire = adhesionShared.prenomGestionnaire;
      this.emailGestionnaire = adhesionShared.emailGestionnaire;
      this.fonctGestionnaire = adhesionShared.fonctGestionnaire;
      this.telGestionnaire = adhesionShared.telGestionnaire;
    }
  }

  constructor(private route: ActivatedRoute,
    private router: Router,
    private sharedDataService: SharedDataService,
    private uploadService: FileUploadService,
    private baseApiService: BaseApiService) {

  }

  step2() {
    this.validateFields();
    if (!this.requiredFielsEmpty) {
      this.adhesion.numRC = this.numRC;
      this.adhesion.raisonSociale = this.raisonSociale;
      this.adhesion.adresse = this.adresse;
      this.adhesion.formeJuridique = this.formeJuridique;
      this.adhesion.ice = this.ice;
      this.adhesion.ifu = this.ifu;
      this.adhesion.cnss = this.cnss;
      this.adhesion.numTaxe = this.numTaxe;
      this.adhesion.nomMandataire = this.nomMandataire;
      this.adhesion.prenomMandataire = this.prenomMandataire;
      this.adhesion.cinMandataire = this.cinMandataire;
      this.adhesion.fonctMandataire = this.fonctMandataire;
      this.adhesion.telMandataire = this.telMandataire;
      this.adhesion.nomGestionnaire = this.nomGestionnaire;
      this.adhesion.prenomGestionnaire = this.prenomGestionnaire;
      this.adhesion.emailGestionnaire = this.emailGestionnaire;
      this.adhesion.fonctGestionnaire = this.fonctGestionnaire;
      this.adhesion.telGestionnaire = this.telGestionnaire;

      this.sharedDataService.adhesion = {};
      this.sharedDataService.adhesion = this.adhesion;

      this.router.navigate(['/step2']);
    }

  }

  validateFields() {
    if (this.ice == undefined || this.ice == '') { 
      this.iceEmpty = true; this.requiredFielsEmpty = true; 
    } else {
      this.iceEmpty = false; this.requiredFielsEmpty = false; 
    }
    if (this.ifu === undefined || this.ifu == '') { 
      this.ifuEmpty = true; this.requiredFielsEmpty = true; 
    } else {
      this.ifuEmpty = false; this.requiredFielsEmpty = false; 
    }
    if (this.cinMandataire === undefined || this.cinMandataire == '') { 
      this.cinEmpty = true; this.requiredFielsEmpty = true; 
    } else {
      this.cinEmpty = false; this.requiredFielsEmpty = false; 
    }
    if (this.emailGestionnaire === undefined || this.emailGestionnaire == '') { 
      this.emailEmpty = true; this.requiredFielsEmpty = true; 
    } else {
      this.emailEmpty = false; this.requiredFielsEmpty = false; 
    }
  }

  cancel() {

    this.sharedDataService.adhesion = null;
    this.router.navigate(['/home']);
  }


  uploadRC(event) {
    this.selectedFiles = event.target.files;
    this.message = '';
    this.currentFile = this.selectedFiles.item(0);
    console.log(this.currentFile)
    this.uploadService.upload(this.currentFile, this.urlUploadRC).subscribe(
      event => {
        if (event instanceof HttpResponse) {
          console.log(event.body)
          if (event.body != null && event.body.errors != null && event.body.errors.length > 0) {
            var listMessages = '';
            for (var i = 0; i < event.body.errors.length; i++) {
              this.message += listMessages.concat(event.body.errors[i] + ' ');
              console.log(i + ' : ' + this.message)
            }

            Swal.fire({
              title: 'Echec',
              html: this.message,
              icon: 'warning'
            })

          } else {
            var res = event.body;

            this.numRC = res.numRC;
            this.raisonSociale = res.raisonSociale;
            this.adresse = res.adresse;
            this.formeJuridique = res.formeJuridique;

            console.log(this.numRC)
          }
        }
      },
      err => {
        Swal.fire({
          title: 'Echéc de chargement',
          html: 'La taille du fichier est trop grande !',
          icon: 'warning'
        })
      });


    this.selectedFiles = undefined;
    this.currentFile = undefined;

  }

  uploadICE(event) {
    this.selectedFiles = event.target.files;
    this.message = '';
    this.currentFile = this.selectedFiles.item(0);
    console.log(this.currentFile)
    this.uploadService.upload(this.currentFile, this.urlUploadICE).subscribe(
      event => {
        if (event instanceof HttpResponse) {
          console.log(event)
          if (event.body != null && event.body.errors != null && event.body.errors.length > 0) {
            var listMessages = '';
            for (var i = 0; i < event.body.errors.length; i++) {
              this.message += listMessages.concat(event.body.errors[i] + ' ');
              console.log(i + ' : ' + this.message)
            }

            Swal.fire({
              title: 'Echec',
              html: this.message,
              icon: 'warning'
            })

          } else {
            var res = event.body;

            this.ice = res.ice;
            this.ifu = res.ifu;
            this.cnss = res.cnss;
            this.numTaxe = res.numTaxe;

          }
        }
      },
      err => {
        Swal.fire({
          title: 'Echéc de chargement',
          html: 'La taille du fichier est trop grande !',
          icon: 'warning'
        })
      });


    this.selectedFiles = undefined;
    this.currentFile = undefined;

  }

}
